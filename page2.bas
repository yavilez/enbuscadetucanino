﻿B4A=true
Group=Default Group
ModulesStructureVersion=1
Type=Activity
Version=8
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.

End Sub

Sub Globals
	Dim  ListView1 As ListView
End Sub

Sub Activity_Create(FirstTime As Boolean)
	Activity.LoadLayout ("extrav")
    ListView1.Initialize ( "ListView1" )
	For  i =  1 To 300
		ListView1.AddSingleLine ( "Item #"  & i)
	Next
	Activity.AddView (ListView1, 0 , 0 ,  100%x ,  100%y )

End Sub
Sub ListView1_ItemClick (Position As Int, Value As Object)
	Activity.Title = Value
End Sub

Sub Activity_Resume

End Sub

Sub Activity_Pause (UserClosed As Boolean)

End Sub

